package com.lyj.housekeepingapi.controller;

import com.lyj.housekeepingapi.model.HouseKeepingRequest;
import com.lyj.housekeepingapi.service.HouseKeepingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/housekeeping")
public class HouseKeepingController {
    private final HouseKeepingService houseKeepingService;

    @PostMapping("/new")
    public String setHouseKeeping(@RequestBody HouseKeepingRequest request) {
        houseKeepingService.setHouseKeeping(request);

        return "OK";
    }
}
