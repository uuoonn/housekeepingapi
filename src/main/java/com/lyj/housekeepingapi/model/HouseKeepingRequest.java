package com.lyj.housekeepingapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class HouseKeepingRequest {

    private Integer aboutAmount;
    private String aboutCategory;
    private String paymentMethod;
    private String aboutDetail;
    private String etcMemo;
}
