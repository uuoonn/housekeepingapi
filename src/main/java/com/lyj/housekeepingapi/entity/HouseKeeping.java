package com.lyj.housekeepingapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.IdGeneratorType;

import java.time.LocalDate;

@Entity
@Getter
@Setter

public class HouseKeeping {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate dateCreat;

    @Column(nullable = false)
    private Integer aboutAmount;

    @Column(nullable = false, length = 6)
    private String aboutCategory;

    @Column(nullable = false, length = 10)
    private String paymentMethod;

    @Column(nullable = false, length = 20)
    private String aboutDetail;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;

}
