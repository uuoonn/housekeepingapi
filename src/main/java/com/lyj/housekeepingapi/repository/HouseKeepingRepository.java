package com.lyj.housekeepingapi.repository;

import com.lyj.housekeepingapi.entity.HouseKeeping;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HouseKeepingRepository extends JpaRepository <HouseKeeping, Long> {
}
