package Enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor //ALL붙는 건 ENUM에서만 쓰인다.
public enum CategoryList {
    SPENDING("지출"),
    FIX_SPENDING("고정 지출"),
    FIX_INCOME("고정 수입"),
    EXTRA_INCOME("부수입");

    private final String name;

}
