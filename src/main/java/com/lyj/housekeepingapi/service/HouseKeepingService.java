package com.lyj.housekeepingapi.service;

import com.lyj.housekeepingapi.entity.HouseKeeping;
import com.lyj.housekeepingapi.model.HouseKeepingRequest;
import com.lyj.housekeepingapi.repository.HouseKeepingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class HouseKeepingService {
    private final HouseKeepingRepository houseKeepingRepository;

    public void setHouseKeeping(HouseKeepingRequest request) {
        HouseKeeping addData = new HouseKeeping();
        addData.setDateCreat(LocalDate.now());
        addData.setAboutCategory(request.getAboutCategory());
        addData.setAboutAmount(request.getAboutAmount());
        addData.setAboutDetail(request.getAboutDetail());
        addData.setPaymentMethod(request.getPaymentMethod());
        addData.setEtcMemo(request.getEtcMemo());

        houseKeepingRepository.save(addData);
    }
}
